import React from 'react';
import Profile from './profile/Profile';
import profileImage from './profile/pic1.jpg';

const App = () => {
  const handleName = (name) => {
    alert(`Profile user's name is ${name}`);
  };

  return (
    <div>
      <Profile
        fullName="Jeff Tchagba"
        bio="Frontend JS Developer"
        profession="Web Development"
        handleName={handleName}
      >
        {profileImage}
      </Profile>
    </div>
  );
};

export default App;
