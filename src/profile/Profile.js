import React from 'react';
import PropTypes from 'prop-types';

const Profile = ({ fullName, bio, profession, children, handleName }) => {
  const styles = {
    container: {
      textAlign: 'center',
      margin: '20px',
    },
    image: {
      width: '200px',
      height: '200px',
      borderRadius: '50%',
      objectFit: 'cover',
      margin: '10px auto',
    },
    fullName: {
      fontSize: '24px',
      fontWeight: 'bold',
      margin: '10px 0',
    },
    bio: {
      fontStyle: 'italic',
      margin: '10px 0',
    },
    profession: {
      fontWeight: 'bold',
      margin: '10px 0',
    },
  };

  return (
    <div style={styles.container}>
      <img src={children} alt="Profile" style={styles.image} />
      <h1 style={styles.fullName}>{fullName}</h1>
      <p style={styles.bio}>{bio}</p>
      <p style={styles.profession}>{profession}</p>
      <button onClick={() => handleName(fullName)}>Voir nom</button>
    </div>
  );
};

Profile.propTypes = {
  fullName: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  profession: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  handleName: PropTypes.func.isRequired,
};

Profile.defaultProps = {
  fullName: 'Jane Doe',
  bio: 'Bio indisponible',
  profession: 'Profession inconnue',
};

export default Profile;